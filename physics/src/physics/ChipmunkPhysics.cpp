//
// Physics.cpp
//
//  Created on: 2018. m�j. 10.
//      Author: fasz
//

#include <physics/ChipmunkPhysics.hpp>

#include <chipmunk/chipmunk.h>

#include <algorithm>
#include <unordered_set>
#include <iterator>

namespace gurgula
{

logger::LoggerToken& operator<<(logger::LoggerToken& lhs, const cpVect& vec)
{
  lhs << vec.x << ':' << vec.y;
  return lhs;
}

namespace physics
{

IPhysics::WorldCoordinate cpVectToWc(cpVect vec)
{
  return {vec.x, vec.y};
}

cpVect wcToCpVect(IPhysics::WorldCoordinate vec)
{
  return {vec.x, vec.y};
}

const IPhysics::WorldUnit UNIT = 0.1;

ChipmunkPhysics::ChipmunkPhysics()
  : mBodyIdGenerator(), mBlockIdGenerator(), mSpace(cpSpaceNew(), cpSpaceFree), mLogger("PHY")
{
  // maybe will need
  // cpSpaceSetDamping
}

void ChipmunkPhysics::update(Duration elapsedTime)
{
  cpSpaceStep(space(), elapsedTime.count() / 1000.0);
  cpSpaceEachBody(space(), (cpSpaceBodyIteratorFunc)bodyIterator, &mLogger);
}

ChipmunkPhysics::BodyWithShapes ChipmunkPhysics::addBody(const BodyDescriptor& definition)
{
  BodyWithShapes ids{mBodyIdGenerator.nextId()};
  auto inserted(
    mBodies.insert(::std::make_pair(ids.body, BodyPointer(cpSpaceAddBody(space(), cpBodyNew(0, 0)), cpBodyFree))));
  cpBody* body(inserted.first->second.get());
  cpBodySetPosition(body, {definition.position.x, definition.position.y});

  for (const auto& block : definition.blocks)
  {
    cpVect transformedShape[MAX_POLYGON_LENGTH];
    ::std::transform(begin(block.shape), end(block.shape), transformedShape, wcToCpVect);
    cpShape* shape(cpSpaceAddShape(space(), cpPolyShapeNewRaw(body, block.shape.size(), transformedShape, 0)));
    cpShapeSetElasticity(shape, 0.0f);
    cpShapeSetFriction(shape, 0.7f);
    cpShapeSetMass(shape, block.mass);

    auto blockId(mBlockIdGenerator.nextId());
    ids.blocks.push_back(blockId);
    mBlocks.insert(::std::make_pair(blockId, ShapePointer(shape, cpShapeFree)));
  }

  return ids;
}

ChipmunkPhysics::BodyData ChipmunkPhysics::queryBody(BodyId id) const
{
  auto body(getBody(id));
  if (body)
  {
    return {cpVectToWc(cpBodyGetPosition(body)), radianToDegree(Radian(cpBodyGetAngle(body)))};
  }
  throw BodyNotFoundException();
}

void ChipmunkPhysics::accelerateBody(BodyId id, WorldCoordinate position, WorldCoordinate force)
{
  cpBodyApplyForceAtLocalPoint(getBody(id), wcToCpVect(force), wcToCpVect(position));
}

ChipmunkPhysics::BodyId ChipmunkPhysics::splitBody(BodyId id, const BlockList& remainders)
{
  ShapeCollector shapes{getBody(id), {}};
  cpSpaceEachShape(space(), (cpSpaceShapeIteratorFunc)collectShapesForBody, &shapes);
  ::std::unordered_set<cpShape*> remainderShapeSet;
  ::std::transform(begin(remainders),
    end(remainders),
    ::std::insert_iterator<::std::unordered_set<cpShape*>>(remainderShapeSet, begin(remainderShapeSet)),
    [this](auto id) { return getShape(id); });

  auto inserted(mBodies.insert(
    ::std::make_pair(mBodyIdGenerator.nextId(), BodyPointer(cpSpaceAddBody(space(), cpBodyNew(0, 0)), cpBodyFree))));
  BodyId newBodyId(inserted.first->first);
  cpBody* newBody(inserted.first->second.get());
  cpBodySetPosition(newBody, cpBodyGetPosition(shapes.body));
  cpBodySetAngle(newBody, cpBodyGetAngle(shapes.body));
  cpVect oldSpeed(cpBodyGetVelocity(shapes.body));
  Radian oldAngularVelocity(cpBodyGetAngularVelocity(shapes.body));
  cpVect oldCenterOfGravity(cpBodyLocalToWorld(shapes.body, cpBodyGetCenterOfGravity(shapes.body)));
  cpBodySetAngularVelocity(shapes.body, 0);

  for (auto shape : shapes.shapes)
  {
    if (0 == remainderShapeSet.count(shape))
    {
      cpSpaceRemoveShape(space(), shape);
      cpShapeSetBody(shape, newBody);
      cpSpaceAddShape(space(), shape);
    }
  }

  // calculate new velocities
  cpVect originalCog(cpBodyLocalToWorld(shapes.body, cpBodyGetCenterOfGravity(shapes.body)));
  cpVect originalBodyAngularVelocity(calculateRotationalSpeed(oldCenterOfGravity, originalCog, oldAngularVelocity));
  cpBodySetVelocity(shapes.body, oldSpeed + originalBodyAngularVelocity);
  cpVect newCog(cpBodyLocalToWorld(newBody, cpBodyGetCenterOfGravity(newBody)));
  cpVect newBodyAngularVelocity(calculateRotationalSpeed(oldCenterOfGravity, newCog, oldAngularVelocity));
  cpBodySetVelocity(newBody, oldSpeed + newBodyAngularVelocity);

  cpBodySetAngularVelocity(shapes.body, oldAngularVelocity);
  cpBodySetAngularVelocity(newBody, oldAngularVelocity);

  return newBodyId;
}

ChipmunkPhysics::BlockShape ChipmunkPhysics::queryBlock(BlockId id) const
{
  auto shape(getShape(id));
  if (shape)
  {
    BlockShape result;
    int vertexCount(cpPolyShapeGetCount(shape));
    for (int i(0); i < vertexCount; ++i)
    {
      result.push_back(cpVectToWc(cpPolyShapeGetVert(shape, i)));
    }
    return result;
  }
  throw BlockNotFoundException();
}

void ChipmunkPhysics::addWall(WorldCoordinate startPoint, WorldCoordinate endPoint)
{
  cpBody* staticBody(cpSpaceGetStaticBody(space()));
  cpShape* wall(cpSpaceAddShape(
    space(), cpSegmentShapeNew(staticBody, cpv(startPoint.x, startPoint.y), cpv(endPoint.x, endPoint.y), 0.0f)));
  cpShapeSetElasticity(wall, 1.0f);
  cpShapeSetFriction(wall, 1.0f);
  mWalls.push_back(ShapePointer(wall, cpShapeFree));
}

void ChipmunkPhysics::collectShapesForBody(cpShape* shape, ShapeCollector* shapeCollector)
{
  if (shapeCollector->body == cpShapeGetBody(shape))
  {
    shapeCollector->shapes.push_back(shape);
  }
}

void ChipmunkPhysics::bodyIterator(cpBody* body, logger::Logger* logger)
{
}

cpVect ChipmunkPhysics::calculateRotationalSpeed(cpVect center, cpVect point, Radian rotation)
{
  return cpvperp(point - center) * rotation.value;
}

cpSpace* ChipmunkPhysics::space()
{
  return mSpace.get();
}

const cpSpace* ChipmunkPhysics::space() const
{
  return mSpace.get();
}

cpBody* ChipmunkPhysics::getBody(BodyId id)
{
  auto iter(mBodies.find(id));
  return end(mBodies) == iter ? 0 : iter->second.get();
}

const cpBody* ChipmunkPhysics::getBody(BodyId id) const
{
  auto iter(mBodies.find(id));
  return end(mBodies) == iter ? 0 : iter->second.get();
}

cpShape* ChipmunkPhysics::getShape(BlockId id)
{
  auto iter(mBlocks.find(id));
  return end(mBlocks) == iter ? 0 : iter->second.get();
}

const cpShape* ChipmunkPhysics::getShape(BlockId id) const
{
  auto iter(mBlocks.find(id));
  return end(mBlocks) == iter ? 0 : iter->second.get();
}

} // namespace physics
} // namespace gurgula
