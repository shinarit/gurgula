#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <physics/ChipmunkPhysics.hpp>

#include <optional>

namespace
{
template<class T>
bool close(T lhs, T rhs, T diff)
{
  return lhs - rhs < diff && rhs - lhs < diff;
}
} // namespace

// TEST_CASE("physics tests", "[GECI]")
TEST_CASE()
{
  ::gurgula::physics::ChipmunkPhysics implementation;
  ::gurgula::physics::IPhysics& physics(implementation);

  const ::gurgula::physics::IPhysics::BlockDescriptor box{{{-5, -5}, {-5, 5}, {5, 5}, {5, -5}}, 1};
  const ::gurgula::physics::IPhysics::BlockDescriptor triangle{{{0, 0}, {10, 0}, {5, 5}}, 10};

  SECTION("body creation")
  {
    ::std::optional<::gurgula::physics::IPhysics::BodyWithShapes> firstBody;
    CHECK_NOTHROW(firstBody = physics.addBody({{triangle, triangle}, {0, 0}}));
    CHECK(2 == firstBody->blocks.size());

    ::std::optional<::gurgula::physics::IPhysics::BodyWithShapes> secondBody;
    CHECK_NOTHROW(secondBody = physics.addBody({{}, {0, 10}}));
    CHECK(0 == secondBody->blocks.size());

    CHECK(!(firstBody->body == secondBody->body));
  }
  SECTION("query properties")
  {
    const ::gurgula::physics::IPhysics::WorldCoordinate center{11, 17};
    ::gurgula::physics::IPhysics::BodyWithShapes ids(physics.addBody({{triangle, triangle}, center}));

    ::gurgula::physics::IPhysics::BodyData bodyData;
    CHECK_NOTHROW(bodyData = physics.queryBody(ids.body));
    CHECK(center == bodyData.position);
    CHECK(::gurgula::Degree(0) == bodyData.rotation);

    ::gurgula::physics::IPhysics::BlockShape blockData;
    CHECK_NOTHROW(blockData = physics.queryBlock(ids.blocks[0]));
    CHECK(triangle.shape == blockData);
    CHECK_NOTHROW(blockData = physics.queryBlock(ids.blocks[1]));
    CHECK(triangle.shape == blockData);
  }
  SECTION("move body")
  {
    const ::gurgula::physics::IPhysics::WorldCoordinate center{11, 17};

    ::gurgula::physics::IPhysics::BodyWithShapes ids(physics.addBody({{box}, center}));

    for (int i(0); i < 100; ++i)
    {
      physics.accelerateBody(ids.body, {0, 0}, {10, 0});
      physics.update(::std::chrono::milliseconds(10));
    }
    ::gurgula::physics::IPhysics::BodyData bodyData(physics.queryBody(ids.body));
    // s = a / 2 * t^2
    CHECK(close(center.x + 5, bodyData.position.x, 0.1));
    CHECK(center.y == bodyData.position.y);
    CHECK(0 == bodyData.rotation.value);

    physics.update(::std::chrono::milliseconds(1000));
    bodyData = physics.queryBody(ids.body);
    // s = v * t
    // v = a * t
    CHECK(close(center.x + 5 + 10, bodyData.position.x, 0.1));
    CHECK(center.y == bodyData.position.y);
    CHECK(0 == bodyData.rotation.value);
  }
  SECTION("rotate body")
  {
    const ::gurgula::physics::IPhysics::WorldCoordinate forcePoint{0, 10};
    //    const ::gurgula::physics::IPhysics::WorldCoordinate forcePoint{0, 0};
    const ::gurgula::physics::IPhysics::WorldCoordinate force{10, 0};
    const ::gurgula::physics::IPhysics::WorldCoordinate center{11, 17};

    ::gurgula::physics::IPhysics::BodyWithShapes ids(physics.addBody({{box}, center}));

    //    for (int i(0); i < 100; ++i)
    //    {
    //      physics.accelerateBody(ids.body, forcePoint, force);
    //      physics.accelerateBody(ids.body, -forcePoint, -force);
    //      //      physics.accelerateBody(ids.body, {0, 0}, {10, 0});
    //      physics.update(::std::chrono::milliseconds(10));
    //    }

    physics.accelerateBody(ids.body, forcePoint, force);
    physics.accelerateBody(ids.body, -forcePoint, -force);
    physics.update(::std::chrono::milliseconds(1000));
    physics.update(::std::chrono::milliseconds(1000));
    ::gurgula::physics::IPhysics::BodyData bodyData(physics.queryBody(ids.body));
    // v = a * t
    // w = c / v
    // u = f * r * t^2
    CHECK(2 * force.x * forcePoint.y == ::gurgula::degreeToRadian(bodyData.rotation).value);
    CHECK(center == bodyData.position);
  }
}
