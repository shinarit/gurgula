//
// Physics.h
//
//  Created on: 2018. m�j. 10.
//      Author: fasz
//

#ifndef PHYSICS_SRC_PHYSICS_CHIPMUNKPHYSICS_HPP_
#define PHYSICS_SRC_PHYSICS_CHIPMUNKPHYSICS_HPP_

#include <physics/IPhysics.hpp>
#include <common/Logger.hpp>

#include <memory>
#include <unordered_map>

class cpSpace;
class cpBody;
class cpShape;
class cpVect;

namespace gurgula
{
namespace physics
{

class ChipmunkPhysics : public IPhysics
{
public:
  ChipmunkPhysics();

  virtual void update(Duration elapsedTime);

  virtual BodyWithShapes addBody(const BodyDescriptor& definition);
  virtual BodyData queryBody(BodyId id) const;
  virtual void accelerateBody(BodyId id, WorldCoordinate position, WorldCoordinate force);
  virtual BodyId splitBody(BodyId id, const BlockList& remainders);

  virtual BlockShape queryBlock(BlockId id) const;

  virtual void addWall(WorldCoordinate startPoint, WorldCoordinate endPoint);

private:
  typedef ::std::unique_ptr<cpSpace, void (*)(cpSpace*)> SpacePointer;
  typedef ::std::unique_ptr<cpBody, void (*)(cpBody*)> BodyPointer;
  typedef ::std::unique_ptr<cpShape, void (*)(cpShape*)> ShapePointer;

  struct ShapeCollector
  {
    cpBody* body;
    ::std::vector<cpShape*> shapes;
  };
  static void collectShapesForBody(cpShape* shape, ShapeCollector* shapeCollector);
  static void bodyIterator(cpBody* body, logger::Logger*);
  static cpVect calculateRotationalSpeed(cpVect center, cpVect point, Radian rotation);

  cpSpace* space();
  const cpSpace* space() const;

  cpBody* getBody(BodyId id);
  const cpBody* getBody(BodyId id) const;

  cpShape* getShape(BlockId id);
  const cpShape* getShape(BlockId id) const;

  IdGenerator mBodyIdGenerator;
  IdGenerator mBlockIdGenerator;

  ::std::unordered_map<BodyId, BodyPointer> mBodies;
  ::std::unordered_map<BlockId, ShapePointer> mBlocks;
  SpacePointer mSpace;
  ::std::vector<ShapePointer> mWalls;

  logger::Logger mLogger;
};

} // namespace physics
} // namespace gurgula

#endif // PHYSICS_SRC_PHYSICS_CHIPMUNKPHYSICS_HPP_
