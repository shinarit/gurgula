//
// IPhysics.hpp
//
//  Created on: 2018. m�j. 10.
//      Author: fasz
//

#ifndef PHYSICS_INCLUDE_PHYSICS_IPHYSICS_HPP_
#define PHYSICS_INCLUDE_PHYSICS_IPHYSICS_HPP_

#include <common/geometry.hpp>
#include <common/id.hpp>

#include <vector>
#include <chrono>

namespace gurgula
{
namespace physics
{

class IPhysics
{
public:
  class PhysicsException
  {
  };
  class BodyNotFoundException : public PhysicsException
  {
  };
  class BlockNotFoundException : public PhysicsException
  {
  };

  typedef double WorldUnit;
  typedef vector<WorldUnit> WorldCoordinate;

  typedef Id BodyId;
  typedef Id BlockId;
  typedef ::std::vector<BlockId> BlockList;
  struct BodyWithShapes
  {
    BodyId body;
    BlockList blocks;
  };

  static const WorldUnit UNIT;

  typedef double Mass;

  // absolute coordinates
  typedef ::std::vector<WorldCoordinate> BlockShape;

  // for creation
  struct BlockDescriptor
  {
    BlockShape shape;
    Mass mass;
  };
  typedef ::std::vector<BlockDescriptor> BlocksDescriptor;
  struct BodyDescriptor
  {
    BlocksDescriptor blocks;
    WorldCoordinate position;
  };

  // for queries
  struct BodyData
  {
    WorldCoordinate position;
    Degree rotation;
  };

  typedef ::std::chrono::milliseconds Duration;
  virtual void update(Duration elapsedTime) = 0;

  virtual BodyWithShapes addBody(const BodyDescriptor& definition) = 0;
  virtual BodyData queryBody(BodyId id) const = 0;
  virtual void accelerateBody(BodyId id, WorldCoordinate position, WorldCoordinate force) = 0;
  virtual BodyId splitBody(BodyId id, const BlockList& remainders) = 0;

  virtual BlockShape queryBlock(BlockId id) const = 0;

  virtual void addWall(WorldCoordinate startPoint, WorldCoordinate endPoint) = 0;
};

} // namespace physics
} // namespace gurgula

#endif // PHYSICS_INCLUDE_PHYSICS_IPHYSICS_HPP_
