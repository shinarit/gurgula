//
// IWindow.h
//
//  Created on: 2018. m�j. 10.
//      Author: fasz
//

#ifndef UI_INCLUDE_UI_IWINDOW_HPP_
#define UI_INCLUDE_UI_IWINDOW_HPP_

#include <common/geometry.hpp>
#include <graphics/IGraphics.hpp>

namespace gurgula
{
namespace ui
{

class IWindow
{
public:
  IWindow(IWindow& parent, const graphics::Box& boundingBox);
  virtual ~IWindow()
  {
  }

protected:
  virtual void drawBox(const graphics::Box& box, graphics::IGraphics::Color color);
  virtual void drawPolygon(const graphics::Polygon& pol,
    graphics::ScreenCoordinate position,
    Degree rotate,
    float scale,
    graphics::IGraphics::Color color);

  const graphics::Box mBoundingBox;

private:
  IWindow& mParent;
};

} // namespace ui
} // namespace gurgula

#endif // UI_INCLUDE_UI_IWINDOW_HPP_
