//
// FreeObserver.hpp
//
//  Created on: 2018. m�j. 26.
//      Author: fasz
//

#ifndef HEE51486E_6D14_4F85_9D08_C5D66545584F
#define HEE51486E_6D14_4F85_9D08_C5D66545584F

#include <ui/IObserver.hpp>
#include <ui/IEventDistributor.hpp>

#include <unordered_map>
#include <functional>

namespace gurgula
{
namespace ui
{

class FreeObserver : public IObserver
{
public:
  struct ControlScheme
  {
    IEventDistributor::KeyCode up;
    IEventDistributor::KeyCode right;
    IEventDistributor::KeyCode down;
    IEventDistributor::KeyCode left;
    IEventDistributor::KeyCode out;
    IEventDistributor::KeyCode in;
  };
  FreeObserver(ControlScheme controls, IEventDistributor& eventDistributor);

  void update();

  // IObserver
  virtual Position getCurrentPosition() const;

private:
  static const int Step = 5;
  void controlEvent(const IEventDistributor::KeyboardEvent& event);
  void move(Coordinate direction);
  void zoom(bool in);

  Position mCurrentPosition = {{0, 0}, 1};

  struct Handler
  {
    bool active;
    typedef ::std::function<void()> Modifier;
    Modifier modifier;
  };
  ::std::unordered_map<IEventDistributor::KeyCode, Handler> mHandlers;
};

} // namespace ui
} // namespace gurgula

#endif // HEE51486E_6D14_4F85_9D08_C5D66545584F
