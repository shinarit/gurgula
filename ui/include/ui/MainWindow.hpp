//
// MainWindow.h
//
//  Created on: 2018. m�j. 10.
//      Author: fasz
//

#ifndef UI_SRC_UI_MAINWINDOW_H_
#define UI_SRC_UI_MAINWINDOW_H_

#include <ui/IWindow.hpp>

#include <ui/IEventDistributor.hpp>
#include <physics/IPhysics.hpp>

// TODO temp
#include <vector>
#include <ui/FreeObserver.hpp>

namespace gurgula
{
namespace ui
{

class MainWindow : public IWindow
{
public:
  MainWindow(graphics::IGraphics& graphics, IEventDistributor& eventDistributor, physics::IPhysics& physics);

  void update();

private:
  virtual void drawBox(const graphics::Box& box, graphics::IGraphics::Color color);
  virtual void drawPolygon(const graphics::Polygon& pol,
    graphics::ScreenCoordinate position,
    Degree rotate,
    float scale,
    graphics::IGraphics::Color color);

  void mouseButtonCallback(const IEventDistributor::MousePressedEvent& event);
  void mouseMoveCallback(const IEventDistributor::MouseMovedEvent& event);
  void keyboardCallback(const IEventDistributor::KeyboardEvent& event);

  graphics::ScreenCoordinate translateCoordinate(physics::IPhysics::WorldCoordinate coord);
  physics::IPhysics::WorldCoordinate translateCoordinate(graphics::ScreenCoordinate coord);

  graphics::IGraphics& mGraphics;
  physics::IPhysics& mPhysics;

  // TODO TEMP
  struct Block
  {
    physics::IPhysics::BlockId id;
    graphics::Polygon shape;
  };
  struct Body
  {
    physics::IPhysics::BodyId body;
    ::std::vector<Block> blocks;
  };
  ::std::vector<Body> mBodies;
  FreeObserver mObserver;
  physics::IPhysics::WorldCoordinate mMousePosition;
  bool mAcceleration = false;
  bool split = false;
  physics::IPhysics::Duration mElapsedTime;
};

} // namespace ui
} // namespace gurgula

#endif // UI_SRC_UI_MAINWINDOW_H_
