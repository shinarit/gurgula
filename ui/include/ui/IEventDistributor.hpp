//
// IEventDistributor.h
//
//  Created on: 2018. �pr. 29.
//      Author: fasz
//

#ifndef UI_INCLUDE_UI_IEVENTDISTRIBUTOR_HPP_
#define UI_INCLUDE_UI_IEVENTDISTRIBUTOR_HPP_

#include <common/id.hpp>

#include <functional>

namespace gurgula
{
namespace ui
{

class IEventDistributor
{
public:
  enum class ButtonStatus
  {
    PRESSED,
    RELEASED
  };
  typedef int MouseButtonId;
  static const int LEFT_MOUSE_BUTTON = 0;
  static const int RIGHT_MOUSE_BUTTON = 1;
  static const int MIDDLE_MOUSE_BUTTON = 2;

  struct MousePressedEvent
  {
    MouseButtonId button;
    ButtonStatus status;
  };

  struct MousePosition
  {
    double x, y;
  };
  struct MouseMovedEvent
  {
    MousePosition cursorPosition;
  };

  typedef int KeyCode;
  struct KeyboardEvent
  {
    KeyCode code;
    ButtonStatus status;
  };

  typedef Id CallbackId;
  typedef std::function<void(const MousePressedEvent&)> MouseButtonEventCallback;
  typedef std::function<void(const MouseMovedEvent&)> MouseMoveEventCallback;
  typedef std::function<void(const KeyboardEvent&)> KeyboardEventCallback;

  virtual CallbackId registerMouseButtonEventHandler(MouseButtonId id, MouseButtonEventCallback callback) = 0;
  virtual CallbackId registerMouseMoveEventHandler(MouseMoveEventCallback callback) = 0;
  virtual CallbackId registerKeyboardEventHandler(KeyCode id, KeyboardEventCallback callback) = 0;
  virtual void unregisterCallback(CallbackId id) = 0;

  virtual void handleEvents() = 0;
};

} // namespace ui
} // namespace gurgula

#endif // UI_INCLUDE_UI_IEVENTDISTRIBUTOR_HPP_
