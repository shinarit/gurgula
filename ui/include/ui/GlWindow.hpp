//
// GlWindow.h
//
//  Created on: 2018. �pr. 28.
//      Author: fasz
//

#ifndef GRAPHICS_INCLUDE_GRAPHICS_GLWINDOW_HPP_
#define GRAPHICS_INCLUDE_GRAPHICS_GLWINDOW_HPP_

#include <graphics/IGraphics.hpp>
#include <ui/IPhysicalWindow.hpp>
#include <ui/IEventDistributor.hpp>

#include <common/Logger.hpp>

#include <unordered_map>
#include <vector>

class GLFWwindow;

namespace gurgula
{
namespace ui
{

class GlWindow
  : public graphics::IGraphics
  , public IPhysicalWindow
  , public ui::IEventDistributor
{
public:
  //
  // BORDERED_WINDOW: size is used
  // BORDERLESS_WINDOW: size is ignored
  // EXCLUSIVE_FULLSCREEN: size who knows
  //
  GlWindow(Mode mode, WindowCoordinate size);
  ~GlWindow();

  // IGraphics
  virtual graphics::ScreenCoordinate size() const;

  virtual void drawBox(const graphics::Box& box, Color color) const;
  virtual void drawPolygon(const graphics::Polygon& pol,
    graphics::ScreenCoordinate position,
    Degree rotate,
    float scale,
    Color color) const;

  // IPhysicalWindow
  virtual bool wantsToDie() const;

  virtual Mode getMode() const;
  virtual bool changeMode(Mode mode);

  typedef int WindowUnit;
  typedef vector<WindowUnit> WindowCoordinate;

  virtual WindowCoordinate getSize() const;
  virtual bool setSize(WindowCoordinate size);

  virtual void startUpdate();
  virtual void finishUpdate();

  // IEventDistributor
  virtual CallbackId registerMouseButtonEventHandler(MouseButtonId id, MouseButtonEventCallback callback);
  virtual CallbackId registerMouseMoveEventHandler(MouseMoveEventCallback callback);
  virtual CallbackId registerKeyboardEventHandler(KeyCode id, KeyboardEventCallback callback);
  virtual void unregisterCallback(CallbackId id);

  virtual void handleEvents();

private:
  static bool GL_INITIALIZED;
  static const char* sBoxVertexShader;
  static const char* sPolygonVertexShader;
  static const char* sColorShader;

  typedef vector<double> OpenGlScreenCoordinate;
  // template<int WindowId>
  struct CallbackHandler
  {
    CallbackHandler();
    void registerHandlers(GlWindow& window);
    static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
    static void cursorPositionCallback(GLFWwindow* window, double xpos, double ypos);
    static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
    static GlWindow* sWindow;
  };

  void init();
  void createWindow(Mode mode, WindowCoordinate size);

  void keyCallback(int key, int scancode, int action, int mods);
  void cursorPositionCallback(double xpos, double ypos);
  void mouseButtonCallback(int button, int action, int mods);

  OpenGlScreenCoordinate translatePixelToDrawScreenCoordinate(graphics::ScreenCoordinate coord) const;

  graphics::ScreenCoordinate mSize;
  IPhysicalWindow::Mode mMode;
  GLFWwindow* mWindow;

  CallbackHandler mCallbackHandler;

  template<class CallbackType>
  struct CallbackStruct
  {
    CallbackId id;
    CallbackType callback;
  };
  ::std::unordered_map<MouseButtonId, ::std::vector<CallbackStruct<MouseButtonEventCallback>>>
    mMouseButtonEventHandlers;
  ::std::unordered_map<CallbackId, MouseMoveEventCallback> mMouseMoveEventHandlers;
  ::std::unordered_map<KeyCode, ::std::vector<CallbackStruct<KeyboardEventCallback>>> mKeyboardEventHandlers;

  IdGenerator mCallbackIdGenerator;

  unsigned int mBoxShaderProgram;
  unsigned int mPolygonShaderProgram;
  unsigned int mBoxVao;
  unsigned int mPolygonVao;
  logger::Logger mLogger;
};

} // namespace ui
} // namespace gurgula

#endif // GRAPHICS_INCLUDE_GRAPHICS_GLWINDOW_HPP_
