//
// FreeObserver.cpp
//
//  Created on: 2018. m�j. 26.
//      Author: fasz
//

#include <ui/FreeObserver.hpp>

namespace gurgula
{
namespace ui
{

FreeObserver::FreeObserver(ControlScheme controls, IEventDistributor& eventDistributor)
  : mHandlers{{controls.up, {false, ::std::bind(&move, this, Coordinate{0, Step})}},
      {controls.right, {false, ::std::bind(&move, this, Coordinate{Step, 0})}},
      {controls.down, {false, ::std::bind(&move, this, Coordinate{0, -Step})}},
      {controls.left, {false, ::std::bind(&move, this, Coordinate{-Step, 0})}},
      {controls.in, {false, ::std::bind(&zoom, this, true)}},
      {controls.out, {false, ::std::bind(&zoom, this, false)}}}
{
  eventDistributor.registerKeyboardEventHandler(controls.up, ::std::bind(&controlEvent, this, std::placeholders::_1));
  eventDistributor.registerKeyboardEventHandler(controls.left, ::std::bind(&controlEvent, this, std::placeholders::_1));
  eventDistributor.registerKeyboardEventHandler(controls.down, ::std::bind(&controlEvent, this, std::placeholders::_1));
  eventDistributor.registerKeyboardEventHandler(
    controls.right, ::std::bind(&controlEvent, this, std::placeholders::_1));
  eventDistributor.registerKeyboardEventHandler(controls.out, ::std::bind(&controlEvent, this, std::placeholders::_1));
  eventDistributor.registerKeyboardEventHandler(controls.in, ::std::bind(&controlEvent, this, std::placeholders::_1));
}

void FreeObserver::update()
{
  for (auto& handler : mHandlers)
  {
    if (handler.second.active)
    {
      handler.second.modifier();
    }
  }
}

FreeObserver::Position FreeObserver::getCurrentPosition() const
{
  return mCurrentPosition;
}

void FreeObserver::controlEvent(const IEventDistributor::KeyboardEvent& event)
{
  bool newState(IEventDistributor::ButtonStatus::PRESSED == event.status);
  mHandlers[event.code].active = newState;
}

void FreeObserver::move(Coordinate direction)
{
  mCurrentPosition.center += (direction / mCurrentPosition.scale);
}

void FreeObserver::zoom(bool in)
{
  mCurrentPosition.scale *= (in ? 1.1 : 0.9);
}

} // namespace ui
} // namespace gurgula
