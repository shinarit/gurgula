//
// MainWindow.cpp
//
//  Created on: 2018. m�j. 10.
//      Author: fasz
//

#include <ui/MainWindow.hpp>

#include <iterator>
#include <algorithm>

namespace gurgula
{
namespace ui
{

MainWindow::MainWindow(graphics::IGraphics& graphics, IEventDistributor& eventDistributor, physics::IPhysics& physics)
  : IWindow(*this, {{0, 0}, {graphics.size().x, graphics.size().y}})
  , mGraphics(graphics)
  , mPhysics(physics)
  , mObserver({87, 68, 83, 65, 81, 69}, eventDistributor)
  , mMousePosition{0, 0}
  , mElapsedTime(0)
{
  eventDistributor.registerMouseButtonEventHandler(
    IEventDistributor::LEFT_MOUSE_BUTTON, ::std::bind(&mouseButtonCallback, this, ::std::placeholders::_1));
  eventDistributor.registerMouseButtonEventHandler(
    IEventDistributor::RIGHT_MOUSE_BUTTON, ::std::bind(&mouseButtonCallback, this, ::std::placeholders::_1));
  eventDistributor.registerMouseButtonEventHandler(
    IEventDistributor::MIDDLE_MOUSE_BUTTON, ::std::bind(&mouseButtonCallback, this, ::std::placeholders::_1));
  eventDistributor.registerMouseMoveEventHandler(::std::bind(&mouseMoveCallback, this, ::std::placeholders::_1));

  physics::IPhysics::BlockShape hexagon{{-200, 0}, {-120, 70}, {-80, 70}, {0, 0}, {-80, -70}, {-120, -70}};
  physics::IPhysics::BlockShape hexagon2{{0, 0}, {80, 70}, {120, 70}, {200, 0}, {120, -70}, {80, -70}};
  auto ids(mPhysics.addBody({{{hexagon, .5}, {hexagon2, .5}}, {0, 0}}));

  mBodies.push_back({ids.body,
    {{ids.blocks[0], graphics::Polygon(begin(hexagon), end(hexagon))},
      {ids.blocks[1], graphics::Polygon(begin(hexagon2), end(hexagon2))}}});
  ids = mPhysics.addBody({{{hexagon, .5}, {hexagon2, .5}}, {400, 0}});
  mBodies.push_back({ids.body,
    {{ids.blocks[0], graphics::Polygon(begin(hexagon), end(hexagon))},
      {ids.blocks[1], graphics::Polygon(begin(hexagon2), end(hexagon2))}}});
}

void MainWindow::update()
{
  IObserver::Position observer(mObserver.getCurrentPosition());

  for (auto& body : mBodies)
  {
    auto bodyData(mPhysics.queryBody(body.body));
    graphics::ScreenCoordinate position(translateCoordinate(bodyData.position));
    drawBox({{position.x - 10, position.y - 10}, {position.x + 10, position.y + 10}}, {0, 255, 255});

    for (auto hex : body.blocks)
    {
      drawPolygon(hex.shape, {position.x, position.y}, bodyData.rotation, observer.scale, {0, 255, 0});
    }
  }

  graphics::ScreenCoordinate position(translateCoordinate(translateCoordinate(translateCoordinate(mMousePosition))));
  drawBox({{position.x - 10, position.y - 10}, {position.x + 10, position.y + 10}}, {255, 255, 255});

  if (mAcceleration)
  {
    //    physics::IPhysics::WorldCoordinate force = {0, 5};
    //    mPhysics.accelerateBody(mBodies[0].body, {100, 0}, force);
    //    mPhysics.accelerateBody(mBodies[0].body, {-100, 0}, -force);
    auto bodyData(mPhysics.queryBody(mBodies[0].body));
    physics::IPhysics::WorldCoordinate force(mMousePosition - bodyData.position);
    mPhysics.accelerateBody(mBodies[0].body, {0, 0}, rotate(force / 10, -bodyData.rotation));
  }
  const ::std::chrono::milliseconds step(100);
  mPhysics.update(step);
  mElapsedTime += step;

  mObserver.update();
}

void MainWindow::drawBox(const graphics::Box& box, graphics::IGraphics::Color color)
{
  mGraphics.drawBox(box, color);
}

void MainWindow::drawPolygon(const graphics::Polygon& pol,
  graphics::ScreenCoordinate position,
  Degree rotate,
  float scale,
  graphics::IGraphics::Color color)
{
  mGraphics.drawPolygon(pol, position, rotate, scale, color);
}

void MainWindow::mouseButtonCallback(const IEventDistributor::MousePressedEvent& event)
{
  if (IEventDistributor::LEFT_MOUSE_BUTTON == event.button)
  {
    if (!split)
    {
      mBodies.push_back({mPhysics.splitBody(mBodies[0].body, {mBodies[0].blocks[0].id}), {mBodies[0].blocks[1]}});
      mBodies[0].blocks.pop_back();
      split = true;
    }
  }
  else if (IEventDistributor::RIGHT_MOUSE_BUTTON == event.button
    && IEventDistributor::ButtonStatus::RELEASED == event.status)
  {
    mAcceleration = !mAcceleration;
  }
}

void MainWindow::mouseMoveCallback(const IEventDistributor::MouseMovedEvent& event)
{
  if (mAcceleration)
  {
    mMousePosition = translateCoordinate(graphics::ScreenCoordinate{event.cursorPosition.x, event.cursorPosition.y});
  }
}

void MainWindow::keyboardCallback(const IEventDistributor::KeyboardEvent& event)
{
}

graphics::ScreenCoordinate MainWindow::translateCoordinate(physics::IPhysics::WorldCoordinate coord)
{
  graphics::ScreenCoordinate screenSize(mGraphics.size());
  IObserver::Position observer(mObserver.getCurrentPosition());
  return {screenSize.x / 2 + observer.scale * (coord.x - observer.center.x),
    observer.scale * (observer.center.y - coord.y) + screenSize.y / 2};
}

physics::IPhysics::WorldCoordinate MainWindow::translateCoordinate(graphics::ScreenCoordinate coord)
{
  graphics::ScreenCoordinate screenSize(mGraphics.size());
  IObserver::Position observer(mObserver.getCurrentPosition());
  return {(coord.x - screenSize.x / 2) / observer.scale + observer.center.x,
    observer.center.y - (coord.y - screenSize.y / 2) / observer.scale};
}

} // namespace ui

} // namespace gurgula
