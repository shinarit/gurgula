//
// IWindow.cpp
//
//  Created on: 2018. m�j. 10.
//      Author: fasz
//

#include <ui/IWindow.hpp>

namespace gurgula
{
namespace ui
{

IWindow::IWindow(IWindow& parent, const graphics::Box& boundingBox): mBoundingBox(boundingBox), mParent(parent)
{
}

void IWindow::drawBox(const graphics::Box& box, graphics::IGraphics::Color color)
{
  mParent.drawBox({box.topLeft + mBoundingBox.topLeft, box.bottomRight + mBoundingBox.topLeft}, color);
}

void IWindow::drawPolygon(const graphics::Polygon& pol,
  graphics::ScreenCoordinate position,
  Degree rotate,
  float scale,
  graphics::IGraphics::Color color)
{
  mParent.drawPolygon(pol, position + mBoundingBox.topLeft, rotate, scale, color);
}

} // namespace ui
} // namespace gurgula
