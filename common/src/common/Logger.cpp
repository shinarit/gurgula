//
// Logger.cpp
//
//  Created on: 2018. m�j. 12.
//      Author: fasz
//

#include <common/Logger.hpp>

#include <iostream>
#include <chrono>
#include <thread>
#include <algorithm>
#include <iterator>

namespace gurgula
{
namespace logger
{

namespace
{
::std::mutex sBufferMutex;
std::chrono::milliseconds UPDATE_FREQUENCY(100);
}
Logger::PrintBuffer Logger::sBuffer;
::std::atomic<int> Logger::sCounter{0};
::std::atomic<bool> Logger::sRunning{true};

LoggerToken::LoggerToken(::std::string subsystem)
{
  stream << '[' << subsystem << ']' << ' ';
}

LoggerToken::~LoggerToken()
{
  ::std::lock_guard<::std::mutex> lock(sBufferMutex);
  Logger::sBuffer.push_back(stream.str());
  ++Logger::sCounter;
}

Logger::Logger(::std::string subsystem): mSubsystem(subsystem)
{
}

LoggerToken Logger::operator()() const
{
  return LoggerToken(mSubsystem);
}

using namespace ::std::chrono_literals;

void Logger::flusher()
{
  while (sRunning)
  {
    ::std::this_thread::sleep_for(UPDATE_FREQUENCY);
    if (0 < sCounter)
    {
      ::std::lock_guard<::std::mutex> lock(sBufferMutex);
      ::std::for_each(begin(sBuffer), end(sBuffer), [](auto& string) { ::std::cout << string << '\n'; });
      sBuffer.clear();
      sCounter = 0;
    }
  }
}

void Logger::stop()
{
  sRunning = false;
}

} // namespace logger
} // namespace gurgula
