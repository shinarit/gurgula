//
// logger.hpp
//
//  Created on: 2018. m�j. 12.
//      Author: fasz
//

#ifndef H0CA9C0A0_62C0_4980_B849_B87B091E8732
#define H0CA9C0A0_62C0_4980_B849_B87B091E8732

#include <sstream>
#include <unordered_set>
#include <mutex>
#include <atomic>
#include <functional>

namespace gurgula
{
namespace logger
{

class LoggerToken
{
public:
  LoggerToken(::std::string subsystem);
  ~LoggerToken();
  template<class T>
  friend const LoggerToken& operator<<(const LoggerToken& token, const T& stuff);

private:
  mutable ::std::ostringstream stream;
};

class Logger
{
public:
  Logger(::std::string subsystem);

  LoggerToken operator()() const;

  static void flusher();
  static void stop();

  // private:
  friend class LoggerToken;

  typedef ::std::vector<::std::string> PrintBuffer;
  static PrintBuffer sBuffer;
  static ::std::atomic<int> sCounter;
  static ::std::atomic<bool> sRunning;

  const ::std::string mSubsystem;
};

template<class T>
const LoggerToken& operator<<(const LoggerToken& token, const T& stuff)
{
  token.stream << stuff;
  return token;
}

} // namespace logger
} // namespace gurgula

#endif // H0CA9C0A0_62C0_4980_B849_B87B091E8732
