//
// geometry.h
//
//  Created on: 2018. �pr. 24.
//      Author: fasz
//

#ifndef COMMON_GEOMETRY_H_
#define COMMON_GEOMETRY_H_

#include <common/Logger.hpp>

#include <vector>
#include <cmath>

namespace gurgula
{
const double pi = 3.1415926535897;

template<class Unit>
struct vector
{
  vector(Unit x = Unit(), Unit y = Unit()): x(x), y(y)
  {
  }
  template<class OtherVectorType>
  vector(const OtherVectorType& vec): x(vec.x), y(vec.y)
  {
  }
  vector& operator=(const vector& vec)
  {
    x = vec.x;
    y = vec.y;
    return *this;
  }
  template<class OtherVectorType>
  vector& operator=(const OtherVectorType& vec)
  {
    *this = vector(vec);
    return *this;
  }
  template<class OtherVectorType>
  vector& operator+=(const OtherVectorType& vec)
  {
    x += vec.x;
    y += vec.y;
    return *this;
  }
  template<class ScalarUnit>
  vector& operator*=(ScalarUnit scalar)
  {
    x *= scalar;
    y *= scalar;
    return *this;
  }
  Unit x, y;
};

template<class Unit>
vector<Unit> operator+(const vector<Unit>& lhs, const vector<Unit>& rhs)
{
  return {lhs.x + rhs.x, lhs.y + rhs.y};
}

template<class Unit>
vector<Unit> operator-(const vector<Unit>& lhs, const vector<Unit>& rhs)
{
  return {lhs.x - rhs.x, lhs.y - rhs.y};
}

template<class Unit, class ScalarUnit>
vector<Unit> operator*(const vector<Unit>& lhs, ScalarUnit rhs)
{
  return {lhs.x * rhs, lhs.y * rhs};
}

template<class Unit, class ScalarUnit>
vector<Unit> operator/(const vector<Unit>& lhs, ScalarUnit rhs)
{
  return {lhs.x / rhs, lhs.y / rhs};
}

template<class Unit>
bool operator==(const vector<Unit>& lhs, const vector<Unit>& rhs)
{
  return lhs.x == rhs.x && lhs.y == rhs.y;
}

template<class Unit>
bool operator!=(const vector<Unit>& lhs, const vector<Unit>& rhs)
{
  return !(lhs == rhs);
}

template<class Unit>
vector<Unit> operator-(const vector<Unit>& vec)
{
  return {-vec.x, -vec.y};
}

template<class T>
const logger::LoggerToken& operator<<(const logger::LoggerToken& lhs, const vector<T>& vec)
{
  return lhs << vec.x << ':' << vec.y;
}

template<class Unit>
struct Rectangle
{
  Unit topLeft, bottomRight;
};

template<class Coordinate>
float distance(const Coordinate& lhs, const Coordinate& rhs)
{
  return ::std::sqrt((lhs.x - rhs.x) * (lhs.x - rhs.x) + (lhs.y - rhs.y) * (lhs.y - rhs.y));
}

enum class MeasurementUnits
{
  Radian,
  Degree
};

template<class BaseType, MeasurementUnits unit>
struct MeasurementUnit
{
  MeasurementUnit(): value()
  {
  }
  explicit MeasurementUnit(BaseType value): value(value)
  {
  }

  operator BaseType()
  {
    return value;
  }

  BaseType value;
};

template<class BaseType, MeasurementUnits unit>
MeasurementUnit<BaseType, unit> operator+(const MeasurementUnit<BaseType, unit>& lhs,
  const MeasurementUnit<BaseType, unit>& rhs)
{
  return MeasurementUnit<BaseType, unit>(lhs.value + rhs.value);
}

template<class BaseType, MeasurementUnits unit>
MeasurementUnit<BaseType, unit> operator-(const MeasurementUnit<BaseType, unit>& mu)
{
  return MeasurementUnit<BaseType, unit>(-mu.value);
}

template<class BaseType, MeasurementUnits unit>
bool operator==(const MeasurementUnit<BaseType, unit>& lhs, const MeasurementUnit<BaseType, unit>& rhs)
{
  return lhs.value == rhs.value;
}

typedef MeasurementUnit<float, MeasurementUnits::Radian> Radian;
typedef MeasurementUnit<float, MeasurementUnits::Degree> Degree;

inline Degree radianToDegree(Radian radian)
{
  return Degree(radian.value / pi * 180.0);
}

inline Radian degreeToRadian(Degree degree)
{
  return Radian(degree.value * pi / 180.0);
}

template<class VectorType>
VectorType rotate(const VectorType& vec, Radian angle)
{
  float cos(::std::cos(angle));
  float sin(::std::sin(angle));

  return VectorType{vec.x * cos - vec.y * sin, vec.x * sin + vec.y * cos};
}

template<class VectorType>
VectorType rotate(const VectorType& vec, Degree angle)
{
  return rotate(vec, degreeToRadian(angle));
}

static const int MAX_POLYGON_LENGTH = 7;

} // namespace gurgula

#endif // COMMON_GEOMETRY_H_
