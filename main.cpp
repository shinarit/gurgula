#include <common/geometry.hpp>
#include <ui/GlWindow.hpp>
#include <ui/include/ui/MainWindow.hpp>
#include <physics/ChipmunkPhysics.hpp>
#include <common/Logger.hpp>

#include <gurgulaConfig.h>

#include <chrono>
#include <thread>

int logDeep(::gurgula::logger::Logger& logger)
{
  logger() << "KACSA";
  return 5;
}

void log(::gurgula::logger::Logger& logger)
{
  logger() << "TESZT " << logDeep(logger) << " VEGE";
}

int main(int argc, char** argv)
{
  ::gurgula::logger::Logger logger("APP");
  ::std::thread loggerThread(::gurgula::logger::Logger::flusher);
  {
    log(logger);
    const int major(gurgula_VERSION_MAJOR);
    const int minor(gurgula_VERSION_MINOR);
    ::gurgula::ui::GlWindow glWindow(::gurgula::ui::IPhysicalWindow::Mode::BORDERED_WINDOW, {1800, 1000});
    ::gurgula::physics::ChipmunkPhysics physics;

    ::gurgula::ui::MainWindow mainWindow(glWindow, glWindow, physics);

    ::std::chrono::milliseconds frameUpdateDelay(10);
    auto previousTime(::std::chrono::steady_clock::now());
    while (!glWindow.wantsToDie())
    {
      glWindow.handleEvents();
      auto currentTime(::std::chrono::steady_clock::now());
      if (currentTime - previousTime > frameUpdateDelay)
      {
        glWindow.startUpdate();
        mainWindow.update();
        glWindow.finishUpdate();
        previousTime = currentTime;
      }
    }
  }
  ::gurgula::logger::Logger::stop();
  loggerThread.join();
}
